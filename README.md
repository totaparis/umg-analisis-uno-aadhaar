# UMG ANALISIS UNO - EJEMPLO AADHAAR

Se debe ejemplificar la planificación y administración de un sistema para su desarrollo y cumplimiento de objetivos mediante **SCRUM** - Proyecto Final de Análisis de Sistemas l - UMG 

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```
