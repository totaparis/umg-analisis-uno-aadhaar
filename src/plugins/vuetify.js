import Vue from 'vue'
import 'vuetify/src/stylus/app.styl'
// Helpers
import colors from 'vuetify/es5/util/colors'
import Vuetify, { VLayout } from 'vuetify/lib'

Vue.use(Vuetify, {
  iconfont: 'md',
  theme: {
    primary: '#6D4C41', // #E53935 //#334878'
    secondary: colors.red.lighten4, // #FFCDD2
    accent: colors.indigo.base // #3F51B5
  },
  components:{
    VLayout
  }
})
