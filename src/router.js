import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import Profile from './views/Profile.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/home',
      name: 'home',
      component: Home
    },
    {
      path: '/profile',
      name: 'profileList',
      component: Home
    },
    {
      path: '/profile/:id',
      name: 'profile',
      component: Profile,
      props: true,
    },
    {
      path: '/dev',
      name: 'dev',
      // route level code-splitting
      // this generates a separate chunk (dev.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "dev" */ './views/Dev.vue')
    }
  ],
  mode:'history'
})
